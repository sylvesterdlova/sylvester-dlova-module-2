void main() {
  //The app which won in the particular sector and the developer of the app

  var app = MTN_App_of_the_year();

  app.name = "EdiTech";
  app.sector = "Educational Solution";
  app.developer = "Ambani";
  app.year = "2021";

  print("Name of the winning ${app.name}");
  print("Sector Won in ${app.sector}");
  print("It was developed by ${app.developer}");
  print("Year which it won ${app.year}");

  app.transformName();
}

//Class containing all app information

class MTN_App_of_the_year {
  String name = " ";
  String? sector, developer, year;

  void transformName() {
    print("App Name to Capital - ${name.toUpperCase()}");
  }
}
